# -*- coding: utf-8 -*-
__author__ = 'ByungJoo'

from bs4 import BeautifulSoup
import urllib2
import csv
import sys
import time


reload(sys)
sys.setdefaultencoding('utf-8')


jongname = []
shcode = []


def get_CSV():
    with open('StockMaster0821_code.csv','rb') as csvfile:
        csvReader = csv.reader(csvfile,delimiter = ',', quotechar = '|' )
        for row in csvReader:
            #name = row[0].decode('cp949')
            # 종목 Code 를 가지고 오는 Source
            code = row[0][1:]

            shcode.append(code)

    getChangeValue()
    #get_Test()

def get_Test():
    for code in shcode:
        financialName = 'Highlights'+code+'.html'
        with open(financialName,'r') as htmlFile:
            html = htmlFile.read()
            soup = BeautifulSoup(html, "html.parser")
            topTitle = soup.find('p', title = '회사정보').text
            #print code + ':' + topTitle
            splitTitle = topTitle.split('|')
            print splitTitle[0][16:]
            print splitTitle[1][7:]
            print splitTitle[2][10:]





def get_html(code):
    sec = 0.5

    #res = urllib2.urlopen('http://media.kisline.com/fininfo/mainFininfo.nice?paper_stock='+code+'&nav=4')
    #time.sleep(sec)
    #html = res.read()
    financialName = 'Highlights'+code+'.html'
    with open(financialName,'r') as htmlFile:
        html = htmlFile.read()

        return html


def extract_chart(code):
    html = get_html(code)

    soup = BeautifulSoup(html, "html.parser")
    topTitle = soup.find('p', title = '회사정보').text


    top50Html = str(topTitle)

    return top50Html

def getChangeValue():
    cnt = 0
    for code in shcode:
        print str(cnt) + ':' + str(code)

        if cnt == 666:
            time.sleep(5)
        elif cnt == 1200:
            time.sleep(5)
        elif cnt == 1600:
            time.sleep(5)


        cnt = cnt + 1

        html = extract_chart(code)
        extract_row(html,code)



shcodeWrite = [] # 종목코드
KIS_IC = []
KRX = []
KSIC_9 = []

def extract_row(html,code):
    realCode = 'A' + code
    shcodeWrite.append(realCode)
    splitTitle = html.split('|')
    print html
    KIS_IC.append(splitTitle[0][16:])
    KRX.append(splitTitle[1][7:])
    KSIC_9.append(splitTitle[2][10:])

    if code == '950130':
        print shcodeWrite

        with open("output.csv", "wb") as f:
            writer = csv.writer(f, delimiter=',')
            plusChange = 0
            for item in shcodeWrite:
                #Write item to outcsv
                writer.writerow([shcodeWrite[plusChange],KIS_IC[plusChange].encode('cp949'),KRX[plusChange].encode('cp949'),KSIC_9[plusChange].encode('cp949')])
                plusChange = plusChange + 1


if __name__ == '__main__':
    get_CSV()
